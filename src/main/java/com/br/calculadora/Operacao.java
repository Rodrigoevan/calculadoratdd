package com.br.calculadora;

import java.util.List;

public class Operacao {

    public static int soma(Integer numeroUm, Integer numeroDois){
        int resultado = numeroUm + numeroDois;
        return resultado;
    }
    public static int soma(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero:numeros) {
            resultado += numero;
        }
        return resultado;
    }

    public static int subtrair(Integer numeroUm, Integer numeroDois){
        int retultado = numeroUm - numeroDois;
        return retultado;
    }
    public static int subtrair(List<Integer> numeros){
        int resultado = numeros.get(0);
        for (int i = 1; i < numeros.size(); i++) {
            resultado -= numeros.get(i);
        }
        return resultado;
    }
    public static int multiplicar(Integer numeroUm, Integer numeroDois){
        int resultado = numeroUm * numeroDois;
        return resultado;
    }

    public static int multiplicar(List<Integer> numeros){
        int resultado = 1;
        for (Integer numero:numeros) {
            resultado *= numero;
        }
        return resultado;
    }
    public static int dividir(Integer numeroUm, Integer numeroDois) {
        if (numeroDois == 0){
            throw new ArithmeticException();
        }
        int resultado = numeroUm / numeroDois;
        return resultado;
    }

}
