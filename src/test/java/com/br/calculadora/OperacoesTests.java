package com.br.calculadora;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTests {

    @Test
    public void testarOperacaoDeSoma(){
        Assertions.assertEquals(Operacao.soma(1,1),2);
    }
    @Test
    public void testarOperacaoDeSomaComLista (){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.soma(numeros),6);
    }

    @Test
    public void testarOperacaoDeSubtracao(){
        Assertions.assertEquals(Operacao.subtrair(2,1),1);
    }

    @Test
    public void testarOperacaoDeSubtracaoLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(14);
        numeros.add(8);
        numeros.add(2);
        Assertions.assertEquals(Operacao.subtrair(numeros),4);
    }

    @Test
    public void testarOperacaoMultiplicacao(){
        Assertions.assertEquals(Operacao.multiplicar(2,4),8);
    }

    @Test
    public void testarOperacaoMultiplicacaoLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(4);
        numeros.add(3);
        Assertions.assertEquals(Operacao.multiplicar(numeros),24);
    }

    @Test
    public void testarOperacaoDivisao(){
        Assertions.assertEquals(Operacao.dividir(4,2),2);
    }

}
